package BetweenProperFractions

class Oracle {
  def Answer() : Long = {
    (4 to 12000).map(u => CountBetween(u).toLong).sum
  }
  
  def CountBetween(n : Int) : Int = {
    val f = (1.0 / 2.0 * n).toInt
    val bottom = (1.0 / 3.0 * n).toInt

    Stream.from(f, -1).takeWhile(u => u>bottom).count(u => Gcd(n, u) == 1)
  }
  
  def Gcd(a: Int,  b : Int): Int ={
    if (a%b==0)
      b
    else Gcd(b, a%b)
  }
  
}
