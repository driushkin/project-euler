package BiggestDigitSum

class Oracle {
  def Answer() : Int =
    (1 to  100).map(u => (1 to 100).map(v => BigInt(u).pow(v).toString().foldLeft(0) (_+_-'0'))).flatten.max

}
