package TriangleRoots;

class Oracle {
  def Answer() : Int = {
    val pc = new Array[Int](1001)
    for(a <- 1 to 1000)
      for(b <- 1 to 1000 - 2 * a) {
        val c = math.sqrt(a*a+b*b).toInt
        if(c*c == a*a+b*b){
          val p = a+b+c
          if(p<=1000)
            pc(p) += 1
        }
    }
    return pc.indexOf(pc.max)
  }
}
