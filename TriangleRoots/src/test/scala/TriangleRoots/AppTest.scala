package TriangleRoots

import org.scalatest.Spec


class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()

    it("my answer!"){
      assert(oracle.Answer() === 840)
    }

    it("array"){
      val p = new Array[Int](1001)
      p(5) += 1

      assert(p(5) === 1)
    }

  }
}