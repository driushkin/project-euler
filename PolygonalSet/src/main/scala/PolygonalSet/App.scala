package PolygonalSet

class Oracle {
  def Answer() : Int = {
    val triangle = Stream.from(1).map(u => u*(u+1)/2).dropWhile(u => u<1000).takeWhile(u => u < 10000)
    val square = Stream.from(1).map(u => u*u).dropWhile(u => u<1000).takeWhile(u => u < 10000)
    val pentagonal = Stream.from(1).map(u => u*(3*u-1)/2).dropWhile(u => u<1000).takeWhile(u => u < 10000)
    val hexagonal = Stream.from(1).map(u => u*(2*u-1)).dropWhile(u => u<1000).takeWhile(u => u < 10000)
    val heptagonal = Stream.from(1).map(u => u*(5*u-3)/2).dropWhile(u => u<1000).takeWhile(u => u < 10000)
    val octagonal = Stream.from(1).map(u => u*(3*u-2)).dropWhile(u => u<1000).takeWhile(u => u < 10000)
    
    for (t <- triangle){
      System.out.println(t)
      for(s <- square){
        System.out.println("s"+s)
        for (p <- pentagonal){
          for(hx <- hexagonal){
            for (hp <- heptagonal){
              for(o <- octagonal){
                if(IsCyclic(List(t, s, p, hx, hp, o)))
                  return t+s+p+hx+hp+o
              }
            }
          }
        }
      }
    }
    
    return 0;
  }
  
  def IsCyclic(l : List[Int]) : Boolean = {
    val n1 = l(0)
    val l2 = l.drop(1)
      for (n2 <- l2) if (CC(n1, n2)){
        val l3 = l2.filter(u => u!=n2)
        for (n3 <- l3) if (CC(n2, n3)){
          val l4 = l3.filter(u => u!=n3)
          for (n4 <- l4) if (CC(n3, n4)){
            val l5 = l4.filter(u => u!=n4)
            for (n5 <- l5) if (CC(n4, n5)){
              val l6 = l5.filter(u => u!=n5)
              if (CC(n5, l6.head) && CC (l6.head, n1)){
                return true;
              }
            }
          }
        }

    }
    false
  }
  
  def CC(n1 : Int, n2: Int) : Boolean = n1%100 == n2/100

}
