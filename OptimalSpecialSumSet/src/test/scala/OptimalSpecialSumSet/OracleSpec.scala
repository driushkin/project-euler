package OptimalSpecialSumSet

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === "20313839404245")
    }
//    it("good set"){
//      assert(oracle.GoodSet(List(1,2,3)) === false)
//      assert(oracle.GoodSet(List(2,3,4)) === true)
//      assert(oracle.GoodSet(List(3,5,6,7)) === true)
//      assert(oracle.GoodSet(List(6,9,11,12,13)) === true)
//      assert(oracle.GoodSet(List(11,17,20,22,23,24)) === true)
//      assert(oracle.GoodSet(List(11,18,19,20,22,25)) === true)
//    }
  }
}