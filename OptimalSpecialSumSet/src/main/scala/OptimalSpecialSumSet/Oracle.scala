package OptimalSpecialSumSet

import collection.mutable

class Oracle {
  def Answer() : String = {
    Stream.from(1).map(u=> FindSet(List[Int](), u)).filter(u=> !u.isEmpty).head.get.reverse.map(u=> u.toString).reduce(_+_)
  }

  def GoodSet(s:List[Int]) : Boolean = {
    var sums = mutable.HashSet[Int]()
    for (ss <- s)
      sums += ss
    var pmax = s.head
    for (i <- 2 to s.length){
      var max = 0
      val combs = s.combinations(i)
      for (comb <- combs) {
        val sum = comb.sum
        if (sum > max)
          max = sum
        if (sum <= pmax)
          return false
        if (sums.contains(sum))
          return false
        sums += sum
      }
      pmax = max
    }
    true
  }

  def GoodSet2(s:List[Int]) : Boolean = {
    var combs = List[List[Int]]()
    for (i <- 1 to s.length){
      combs = combs ++ s.combinations(i).toList
    }
    for(com1 <- combs; com2 <- combs){
      if (com1.intersect(com2).isEmpty){
        if (com1.sum == com2.sum)
          return false
        if (com1.length > com2.length && com1.sum <= com2.sum)
          return false
        if (com2.length > com1.length && com2.sum <= com1.sum)
          return false
      }
    }
    true
  }

  def FindSet(s : List[Int], r : Int) : Option[List[Int]] = {
    val nmax = 7

    if (s.length == nmax-1) {
      if (r <= s.head)
        return Option.empty
      if (GoodSet(r :: s))
        return Option(r :: s)
      return Option.empty
    }
    if (s.isEmpty){
      for (i <- 10 to r - nmax) {
        val res = FindSet(i :: s, r - i)
        if (!res.isEmpty)
          return res
      }
    }
    else {
      for (i <- s.head+1 to r - s.head) {
        val res = FindSet(i :: s, r - i)
        if (!res.isEmpty)
          return res
      }
    }
    Option.empty
  }

}
