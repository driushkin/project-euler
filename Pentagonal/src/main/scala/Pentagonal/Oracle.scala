package Pentagonal;

class Oracle {
  def Answer() : Int = {
    for(i <- 1 to  2000){
      val d = Pen(i)
      for (j <- i+1 to i * 4/3 ){
        val k = Pen(j)
        if(isPen(k - d) && isPen (2*k-d)) {
          return d;
        }
      }
    }

    return 0;
  }

  def Pen(n : Int) = n * (3*n-1) / 2

  def isPen(n : Int) = (math.sqrt(1+24*n).toInt*math.sqrt(1+24*n).toInt == 1+24*n) &&
    (math.sqrt(1+24*n) +1).toInt % 6 == 0

}
