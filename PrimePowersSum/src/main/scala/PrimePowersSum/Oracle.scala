package PrimePowersSum

import collection.mutable.BitSet

class Oracle {

  def Answer() : Int = {
    Calc(50000000);
  }

  def Calc(N : Int): Int = {
    var expr = BitSet()
    Primes().takeWhile(u=> u*u +8+16 < N).foreach(a1 => {
      Primes().takeWhile(u=> u*u*u +a1*a1+16 < N).foreach(a2 => {
        Primes().takeWhile(u=> u*u*u*u +a1*a1+a2*a2*a2 < N).foreach(a3 => {
          expr += (a1*a1+a2*a2*a2+a3*a3*a3*a3)
        })
      })
    })
    expr.size
  }

  def IsPrime(n: Int): Boolean = {
    for (i <- 2 to math.sqrt(n).toInt)
      if (n%i==0)
        return false;
    true;
  }

  def Primes() : Stream[Int] = {
    Stream.from(2).filter(u=> IsPrime(u))
  }

}
