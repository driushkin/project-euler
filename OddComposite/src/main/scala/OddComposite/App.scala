package OddComposite;

class Oracle  {
  def Answer(): Int = {
    for (i <- 3 to 100000000 by 2){
      if(!isPrime(i)){
        if(!canExpress(i))
          return i;
      }
    }
    return 0
  }

  def canExpress(n : Int) : Boolean = {
    for (p <- 2 to n){
          if(isPrime(p)){
            val left = n - p
            if(left % 2 == 0){
              if(math.sqrt(left/2).toInt * math.sqrt(left/2).toInt == left/2)
                return true;
            }
          }
        }
    return false;
  }

  def isPrime(n : Int) : Boolean = {
    for (i <- 2 to math.sqrt(n).toInt )
      if(n % i == 0)
        return false;
    return true;
  }

}
