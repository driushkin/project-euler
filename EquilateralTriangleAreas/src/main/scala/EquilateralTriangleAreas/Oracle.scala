package EquilateralTriangleAreas

class Oracle {
  def Answer() : Long =
    (3 to (1000000000+2)/3).filter(u=>IsIntArea(u, u-1)).map(u=> u*3-2).sum +
      (3 to (1000000000+2)/3).filter(u=>IsIntArea(u, u+1)).map(u=> u*3+2).sum

  def IsSquare(l: Long) : Boolean = {
    val d = math.sqrt(l).toLong
    d*d==l
  }

  def IsIntArea(s1: Long, s2 : Long) : Boolean = {
    if (s1 % 2 != 0)
      return false
    IsSquare(s2*s2-s1*s1/4)
  }


}
