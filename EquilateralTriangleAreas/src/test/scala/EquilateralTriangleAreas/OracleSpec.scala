package EquilateralTriangleAreas

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 518408346)
    }

    it("area"){
      assert(oracle.IsIntArea(6, 5) === true)
      assert(oracle.IsSquare(971562018622543936L) === true)
    }

  }
}