package ConsecutiveIntegerFactors;


class Oracle {
  def Answer() : Int = {
    var okPrimes = 0
    for( i <- Stream.from(1) ){
      if(primeFactors(i) == 4)
        okPrimes += 1
      else
        okPrimes = 0
      if(okPrimes == 4)
        return i - 3
    }
    return 0;
  }

  def primeFactors(n : Int) : Int = {
    var num = 0
    var nn = n
    for (i <- 2 to n){
      if(nn % i == 0){
        num += 1
        while(nn % i == 0)
          nn /= i
      }
    }
    return num
  }

}
