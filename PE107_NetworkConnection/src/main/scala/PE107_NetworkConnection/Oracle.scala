package PE107_NetworkConnection

class Oracle {
  def Answer() : Int = {
    val arr = ReadInput()
    NetworkWeight(arr) - OptimalNetworkWeight(arr)
  }

  def ReadInput(): Array[Array[Int]] = {
    io.Source.fromFile("network.txt").getLines()
      .map(ln => ln.split(',').map(n => if (n == "-") Int.MaxValue else n.toInt).toArray).toArray
  }

  def NetworkWeight(m : Array[Array[Int]]) : Int =
    m.map(n => n.filter(p => p != Int.MaxValue).sum).sum / 2

  def OptimalNetworkWeight(m : Array[Array[Int]]) : Int = {
    var sum = 0
    var visited = List[Int](0)
    var remaining = (1 to m.length -1).toSet
    while(!remaining.isEmpty){
      val me = (for (i <- visited; j <- remaining) yield (i, j))
        .minBy(u => m(u._1)(u._2))
      sum += m(me._1)(me._2)
      remaining = remaining - me._2
      visited = me._2 :: visited
    }
    sum
  }

}
