package PE109_DartsCheckouts

class Oracle {
  def Answer() : Int = {
    (for(m1 <- 1 to 3; v1 <- ((0 to 20):+25); m2 <- 1 to 3; v2 <- (0 to 20):+25; v3 <- (1 to 20):+25
    if ((new Hit(m1, v1).Score() <= new Hit(m2, v2).Score()) && !((m1 == 3 && v1 == 25) || (m2 == 3 && v2==25))))
        yield new Checkout(new Hit(m1, v1), new Hit(m2, v2), new Hit(2, v3))
    ).distinct.filter(p => p.Score() < 100).length
  }
}

class Checkout(hit1 : Hit, hit2: Hit, hit3: Hit) {
  def Score() = hit1.Score() + hit2.Score() + hit3.Score()
  def Hit1() = hit1
  def Hit2() = hit2
  def Hit3() = hit3

  override def equals(other:Any) : Boolean = {
    if(!other.isInstanceOf[Checkout])
      return false
    val o = other.asInstanceOf[Checkout]
    o.Hit3 == hit3 && ( (o.Hit1== hit1 && o.Hit2==hit2)||(o.Hit1==hit2 && o.Hit2==hit1) )
  }

  override def hashCode() = hit1.hashCode() ^ hit2.hashCode()*98767 ^ hit3.hashCode()*54677467
}

class Hit(mult : Int, points: Int){
  def Score() = mult * points
  def Mult() = mult
  def Points() = points
  override def equals(other:Any) : Boolean = {
    if(!other.isInstanceOf[Hit])
      return false
    val o = other.asInstanceOf[Hit]
   (o.Mult == mult && (o.Points() == points)) || (o.Points() == 0 && Points() == 0)
  }

  override def hashCode() = {
    if (points == 0) 0.hashCode()
    else (mult * 1997 * points).hashCode()
  }

}

