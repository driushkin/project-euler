package PE108_DiophantineEquations

class Oracle {
  def Answer() : Int = {
    return Stream.from(1).filter(n => NumEquations(n) > 1000).head
  }

  def NumEquations(n : Long) : Int =
    (n+1L to n*2L).count(a => (a * n) % (a - n) == 0L)

}
