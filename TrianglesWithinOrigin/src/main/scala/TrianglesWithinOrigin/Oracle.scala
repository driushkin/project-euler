package TrianglesWithinOrigin

class Oracle {
  def Answer() : Int = {
    io.Source.fromFile("triangles.txt").getLines().map(ln => ln.split(','))
    .map(l => AngleSum(l(0).toInt, l(1).toInt, l(2).toInt, l(3).toInt, l(4).toInt, l(5).toInt))
    .filter(p => p >= math.Pi*2 - 0.00000001)
    .length
  }

  def AngleSum(x1: Int, y1: Int, x2 : Int, y2 : Int, x3 : Int, y3: Int) : Double = {
    Angle(x1, y1, x2, y2) + Angle(x1, y1, x3, y3) + Angle(x2, y2, x3, y3)
  }

  def Angle(x1: Int, y1: Int, x2:Int, y2 : Int) : Double = {
    math.acos((x1*x2+y1*y2)/math.sqrt(x1*x1+y1*y1)/math.sqrt(x2*x2+y2*y2))
  }

}
