package CubePermutations

import collection.mutable.{HashMap}

class Oracle {
  val CanonicalCache = HashMap[String, Int]()

  def Answer() : BigInt = {
    val cubeRange = (1 to 10000).map(u => u.toLong*u*u)
    cubeRange.foreach(u=> RegisterCube(u))
    cubeRange.filter(u => CanonicalCache(u.toString.sorted) == 5).head
  }
  


  def RegisterCube(n : Long) = {
    val canon = n.toString.sorted
    if (CanonicalCache.contains(canon))
      CanonicalCache(canon) = CanonicalCache(canon)+1
    else
      CanonicalCache(canon) = 1
  }
}
