package MatrixMinPath

class Oracle {
  def Answer() : Int = {
    ReadData()
    FloydWarshall()
  }
  val N = 80
  val NN = N*N
  
  var m = Array.ofDim[Int](N, N)
  var path = Array.fill(NN, NN){1000000}

  def FloydWarshall() : Int = {
    InitPath()
    for (k<-0 until NN) {
      //System.out.println(k)
      for (i<- 0 until NN){
        for (j<- 0 until NN){
          path(i)(j) = math.min(path(i)(j), path(i)(k)+path(k)(j))
        }
      }
    }
    path(0)(NN-1)+path(0)(0)
  }

  def PathIndex(x:Int, y:Int) = x*N+y

  def AddPath(ai: Int, aj: Int, bi: Int, bj: Int) {
    if (InBounds(ai, aj) && InBounds(bi, bj)){
      path(PathIndex(ai, aj))(PathIndex(bi, bj)) = m(bi)(bj)
      path(PathIndex(ai, bj))(PathIndex(ai, aj)) = m(ai)(aj)
    }
  }

  def InitPath() {
    for (ai <- 0 until N) {
      for (aj <- 0 until N) {
        AddPath(ai, aj, ai-1, aj)
        AddPath(ai, aj, ai+1, aj)
        AddPath(ai, aj, ai, aj-1)
        AddPath(ai, aj, ai, aj+1)
      }
    }
  }

  def InBounds(x:Int, y:Int) = x>= 0 && x< 80 && y>= 0 && y<80
  
  def ReadData(){
    var i = 0
    for(ln <- io.Source.fromFile("matrix.txt").getLines()){
      val ll = ln.split(",")
      for(j <- 0 to ll.length - 1)
        m(i)(j) = ll(j).toInt
      i+= 1
    }
  }
}
