package SpecialSumSubsets

class Oracle {
  def Answer() : Int = {
    io.Source.fromFile("sets.txt").getLines().map(u => u.split(','))
    .map(u => u.map(v => v.toInt).toList)
    .filter(u => GoodSet2(u)).map(n => n.sum).sum
  }

  def GoodSet2(s:List[Int]) : Boolean = {
    var combs = List[List[Int]]()
    for (i <- 1 to s.length){
      combs = combs ++ s.combinations(i).toList
    }
    for(com1 <- combs; com2 <- combs){
      if (com1.intersect(com2).isEmpty){
        if (com1.sum == com2.sum)
          return false
        if (com1.length > com2.length && com1.sum <= com2.sum)
          return false
        if (com2.length > com1.length && com2.sum <= com1.sum)
          return false
      }
    }
    true
  }

}
