package TotientPermutation

class Oracle {
  def Answer() : Int = {
    (2 to 10000000).par.map(u => (u, Phi(u)))
      .filter(u => u._1.toString.sorted == u._2.toString.sorted)
      .minBy(u => u._1.toDouble/u._2)._1
  }

  def Phi(n : Int) : Int = {
      val d = Divisors(n)
      if (d.length == 0)
        return n-1

      val c = Combinations(d)
      n - c.map(u => (if(u.length % 2 == 0) -1 else 1) * n / u.product).sum
    }

    def Combinations(l : List[Int]) : List[List[Int]] =
      (1 to l.size).flatMap(l.combinations).toList

    def Divisors(n: Int) : List[Int] = {
      var res = List[Int]()
      var nn = n
      var i = 2
      while(i <= math.sqrt(nn).round.toInt){
        if(nn%i == 0){
          res = res :+ i
          while(nn%i==0)
            nn=nn/i
        }
        i += 1
      }
      if(nn != 1)
        res = res :+ nn
      res
    }

}
