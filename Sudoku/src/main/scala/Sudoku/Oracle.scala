package Sudoku

class Oracle {

  def Answer() : Int =
    io.Source.fromFile("sudoku.txt").getLines().grouped(10)
      .map(u=> new SudokuBoard(u.drop(1).toList).Solve()).sum
}

class SudokuBoard(val board : Array[Array[Int]]) {
  def this(brd : List[String]) =
    this(Array.tabulate(9, 9)((x, y) => brd(x)(y).toInt-'0'.toInt ))

  def Solve() : Int = {
    val nx = NextFree().get
    CheckStep(nx._1, nx._2)
    board(0)(0)*100+board(0)(1)*10+board(0)(2)
  }

  def ConstraintsOk(): Boolean = {
    for (i <- 0 to 8){
      val n = board(i).filter(u=> u != 0)
      if (n.distinct.size != n.size)
        return false
    }
    for (i <- 0 to 8){
      val n = (0 to 8).map(j=>board(j)(i)).filter(u=> u != 0)
      if (n.distinct.size != n.size)
        return false
    }

    for (i<- 0 to 2; j <- 0 to 2) {
      var n = List[Int]()
      for (ii<- 0 to 2; jj<- 0 to 2){
        n = board(i*3+ii)(j*3+jj) :: n
      }
      n = n.filter(u=> u != 0)
      if (n.distinct.size != n.size)
        return false
    }

    true
  }

  def CheckStep(x: Int, y : Int) : Boolean = {
    for( i<- 1 to 9) {
      board(x)(y) = i
      if (ConstraintsOk()){
        val nx = NextFree()
        if (nx.isEmpty)
          return true
        if (CheckStep(nx.get._1, nx.get._2))
          return true
      }
    }
    board(x)(y) = 0
    false
  }

  def NextFree() : Option[(Int, Int)] = {
    for(x<- 0 to 8; y <- 0 to 8){
      if (board(x)(y)==0)
        return Option[(Int, Int)]((x, y))
    }
    Option.empty
  }


}