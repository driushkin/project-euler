package PhiMax

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 510510)
    }
    it("try"){
      assert(oracle.Phi(6) === 2)
      assert(oracle.Phi(8) === 4)
      assert(oracle.Phi(30) === 8)
      assert(oracle.Phi(92) === 44)
      assert(oracle.Phi(97) === 96)
      assert(oracle.Phi(99) === 60)
      assert(oracle.Phi(87109) === 79180)
     // var l = List[Int](2,3,5)
     // System.out.println((1 to l.size).flatMap(l.combinations) )
    }
  }
}