package Poker

import java.io.{FileReader, BufferedReader};

class Oracle  {
  def Answer() : Int = {
    ReadHands()
      .map( u=> SplitHand(u) )
      .filter(u => u._1.IsGreater(u._2))
      .length
  }
  
  def ReadHands(): List[String] = {
    var res = List[String]()
    val rd = new BufferedReader(new FileReader("poker.txt"))
    var l : String = null
    do {
      l = rd.readLine()
      if(l!=null)
        res = res :+ l
    } while(l != null)
    rd.close()
    res
  }
  
  def SplitHand(hand : String) : (Hand,  Hand) =
    (new Hand(SplitCards(hand.substring(0, 14))), new Hand(SplitCards(hand.substring(15, 29))))

  def SplitCards(hand : String): List[Card] =
    hand.split(" ").map(u => new Card(u)).toList

}

class Hand(val h : List[Card])
{
  val r = h.map(u=> u.Rank()).sorted

  def IsGreater(other : Hand):Boolean={
    val hc1 = HighestComb()
    val hc2 = other.HighestComb()

    for(i <- 0 to 5){
      if(hc1(i) > hc2(i))
        return true;
      else if(hc1(i) < hc2(i))
        return false
    }

    throw new Exception();
  }

  def HighestComb() : List[Int] = {
    List[List[Int]](RoyalFlushEval(), StraightFlushEval(), FourOfAKindEval(), FullHouseEval(), FlushEval(),
      StraightEval(), ThreeOfAKindEval(), TwoPairsEval(), OnePairEval(), HighCardEval())
      .zipWithIndex.filter(u => u._1 != null)
      .map(u => List[Int](-u._2) ++ u._1 )
      .head
  }
  
  def HighCardEval() : List[Int] =
    List[Int](r(4), r(3), r(2), r(1), r(0))

  def OnePairEval() : List[Int] = {
    if (r(0) == r(1)) List[Int](r(0), r(4), r(3), r(2))
    else if (r(1) == r(2)) List[Int](r(1), r(4), r(3), r(0))
    else if (r(2) == r(3)) List[Int](r(2), r(4), r(1), r(0))
    else if (r(3) == r(4)) List[Int](r(3), r(2), r(1), r(0))
    else null
  }

  def TwoPairsEval() : List[Int] = {
    if (r(0)==r(1) && r(2)==r(3)) List[Int](r(2), r(1), r(4))
    else if (r(1)==r(2) && r(3)==r(4)) List[Int](r(3), r(1), r(0))
    else if (r(0)==r(1) && r(3)==r(4)) List[Int](r(3), r(1), r(2))
    else null
  }
  
  def ThreeOfAKindEval() : List[Int] = {
    if(r(0)==r(1) && r(1)==r(2) ) List[Int](r(0), r(4), r(3))
    else if (r(1)==r(2) && r(2)==r(3)) List[Int](r(1), r(4), r(0))
    else if (r(2)==r(3) && r(3)==r(4)) List[Int](r(2), r(1), r(0))
    else null
  }

  def StraightEval() : List[Int] = {
    if (r(4)-r(3) == 1 && r(3)-r(2) == 1 && r(2)-r(1)==1 && r(1)-r(0)==1)
      List[Int](r(4))
    else null
  }

  def FlushEval() : List[Int] = {
    if(h.map(u => u.Suit()).distinct.length == 1)
      List[Int](r(4), r(3), r(2), r(1), r(0))
    else null
  }

  def FullHouseEval() : List[Int] = {
    if (r(0)==r(1) && r(1)==r(2) && r(3)==r(4))
      List[Int](r(2), r(4))
    else if(r(0)==r(1) && r(2)==r(3) && r(3)==r(4))
      List[Int](r(3), r(0))
    else null
  }

  def FourOfAKindEval() : List[Int] = {
    if(r(0)==r(1) && r(1)==r(2) && r(2)==r(3))
      List[Int](r(0), r(4))
    else if (r(1)==r(2) && r(2)==r(3) && r(3)==r(4))
      List[Int](r(1), r(0))
    else null
  }

  def StraightFlushEval() : List[Int] = {
    if(StraightEval()!=null && FlushEval()!=null)
      StraightEval()
    else null
  }

  def RoyalFlushEval() : List[Int] = {
    if(StraightFlushEval() != null && r(4) == 14)
      List[Int](r(4))
    else null
  }

}

class Card(val card : String)
{
  def Rank():Int = {
    card(0) match{
      case '2' => 2
      case '3' => 3
      case '4' => 4
      case '5' => 5
      case '6' => 6
      case '7' => 7
      case '8' => 8
      case '9' => 9
      case 'T' => 10
      case 'J' => 11
      case 'Q' => 12
      case 'K' => 13
      case 'A' => 14
      case _ => throw new Exception()
    }
  }
  
  def Suit() : Int = {
    card(1) match {
      case 'C' => 0
      case 'S' => 1
      case 'D' => 2
      case 'H' => 3
      case _ => throw new Exception()
    }
  }
  
}
