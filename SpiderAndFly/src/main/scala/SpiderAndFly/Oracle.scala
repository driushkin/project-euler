package SpiderAndFly

class Oracle {
  def Answer() : Int =
    Stream.from(1).takeWhile(u => PathsWithInteger(u) < 1000000).last + 1

  def IsSquare(i: Int): Boolean = {
    var sq = math.sqrt(i).toInt
    sq*sq == i
  }

  def SquarePath(i: Int, i1: Int, i2: Int): Boolean =
    IsSquare(i2*i2+(i+i1)*(i+i1))

  def PathsWithInteger(M: Int) : Int = {
    var sum = 0
    for (a1 <- 1 to M){
      for (a2 <-a1 to M) {
        for (a3 <- a2 to M ){
          if(SquarePath(a1, a2, a3)) {
            sum += 1
          }
        }
      }
    }
    System.out.println(M+":"+sum)
    sum
  }

}
