package SpiderAndFly

import org.scalatest.FunSpec


class OracleSpec extends FunSpec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 1818)
    }
  }
}