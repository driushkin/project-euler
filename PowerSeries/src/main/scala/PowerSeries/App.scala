package PowerSeries

class Oracle  {
  def Answer(): BigInt = {
    var sum = BigInt(0)
    for(i<- 1 to 1000){
      var ii = BigInt(i)
      for (j <- 1 to i-1)
        ii = ii * i
      sum += ii
    }
    return sum
  }
}
