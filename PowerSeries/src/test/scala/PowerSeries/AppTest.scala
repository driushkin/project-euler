package PowerSeries

import org.scalatest.Spec
;


class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer"){
      val ans = oracle.Answer().toString()
      assert(ans.substring(ans.length()-10, ans.length()) === "9110846700")
    }
  }
    

}
