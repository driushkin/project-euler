package CombinationsOverMillion

import org.scalatest.Spec


class OracleSpec extends Spec{
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 4075)
    }

    it("try"){
      assert(oracle.Comb(5, 3) === 10)
    }

  }

}