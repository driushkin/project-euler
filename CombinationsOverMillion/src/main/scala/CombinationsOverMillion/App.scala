package CombinationsOverMillion

class Oracle {


  def Answer() : Int ={
    (1 to 100).map( u =>
      (1 to  u).map(v => Comb(u,v)).filter(v => v > 1000000).length
    ).sum
  }

  def Comb(n : Int,  r : Int) : BigInt =
    (1 to n-r).foldLeft((1 to r).foldLeft((1 to n).foldLeft(BigInt(1))(_*_))(_ / _))(_ / _)

}
