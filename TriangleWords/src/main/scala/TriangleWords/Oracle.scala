package TriangleWords

import java.io.FileInputStream
import java.nio.channels.FileChannel
import java.nio.charset.Charset
;

class Oracle  {
  def Answer() : Int = {
    return splitWords(readData()).count( u => isTriangle(wordValue(u)) )
  }

  def readData() : String = {
    val fis = new FileInputStream("words.txt")
      val fc = fis.getChannel()
      val bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size())
      return Charset.defaultCharset().decode(bb).toString()
  }

  def splitWords(str: String) : Array[String] =
    str.split(",").map(s => s.trim().stripPrefix("\"").stripSuffix("\"") )

  def wordValue(word : String) : Int =
    word.foldLeft(0)( (u, v) => u + v.toInt - 'A'.toInt + 1 )


  def isTriangle(x : Int) : Boolean = {
    val det = 1 + 8*x
    val sqrn = math.sqrt(det).intValue()
    return (sqrn%2==1) && (sqrn*sqrn == det)
  }

}
