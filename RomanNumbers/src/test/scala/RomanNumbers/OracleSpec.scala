package RomanNumbers

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 743)
    }

    it("romal len"){
      assert(oracle.RomanLen(1) === 1)
      assert(oracle.RomanLen(2) === 2)
      assert(oracle.RomanLen(3) === 3)
      assert(oracle.RomanLen(4) === 2)
      assert(oracle.RomanLen(5) === 1)
      assert(oracle.RomanLen(6) === 2)
      assert(oracle.RomanLen(7) === 3)
      assert(oracle.RomanLen(8) === 4)
      assert(oracle.RomanLen(9) === 2)
      assert(oracle.RomanLen(10) === 1)
      assert(oracle.RomanLen(19) === 3)
      assert(oracle.RomanLen(49) === 4)
      assert(oracle.RomanLen(1606) === 5)
    }

    it("parse roman"){
      assert(oracle.ParseRoman("I") === 1)
      assert(oracle.ParseRoman("II") === 2)
      assert(oracle.ParseRoman("III") === 3)
      assert(oracle.ParseRoman("IIII") === 4)
      assert(oracle.ParseRoman("IV") === 4)
      assert(oracle.ParseRoman("VVIIIIII") === 16)
      assert(oracle.ParseRoman("MCCCCCCVI") === 1606)
      assert(oracle.ParseRoman("XLIX") === 49)
      assert(oracle.ParseRoman("XXXXVIIII") === 49)
    }

    it("higher"){
      assert(oracle.ContainsHigher('I', 1, "XIV") === true)
    }

  }
}