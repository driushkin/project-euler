package RomanNumbers

class Oracle {
  def Answer() : Int = {
    val lines = scala.io.Source.fromFile("roman.txt").getLines()
    lines.map(l => l.length - RomanLen(ParseRoman(l))).sum
  }

  def RomanLen(n : Int) : Int = {
    val (s1, n1) = RomanReduce(n, 1000, 900, 500, 400, 100)
    val (s2, n2) = RomanReduce(n1, 100, 90, 50, 40, 10)
    val (s3, n3) = RomanReduce(n2, 10, 9, 5, 4, 1)

    s1+s2+s3+n3
  }

  def RomanReduce(n : Int, step : Int, st : Int, step2: Int, st2:Int, s : Int) : (Int, Int) = {
    var sum = 0
    var nn = n
    while(nn >= st){
      nn -= step
      sum+= 1
    }
    while(nn < 0){
      nn += s
      sum += 1
    }
    if (nn >= st2){
      nn -= step2
      sum += 1
    }
    while(nn < 0){
      nn += s
      sum += 1
    }
    (sum, nn)
  }

  def ParseRoman(r : String) : Int =
    r.zipWithIndex.map( u => ToNum(u._1) * (if (ContainsHigher(u._1, u._2, r)) -1 else 1) )
    .sum

  def ContainsHigher(c : Char, i : Int, s : String) : Boolean =
    !s.drop(i).map(ToNum).filter(u => u > ToNum(c)).isEmpty

  def ToNum(c : Char) : Int =
    c match {
      case 'I' => 1
      case 'V' => 5
      case 'X' => 10
      case 'L' => 50
      case 'C' => 100
      case 'D' => 500
      case 'M' => 1000
    }

}

