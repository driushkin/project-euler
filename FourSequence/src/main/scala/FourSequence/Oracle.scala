package FourSequence;

class Oracle  {
  def Answer(): Long = {
    for (d1 <- 0 to 9){
      for (d2 <- d1 to 9){
        for (d3 <- d2 to 9){
          for (d4 <- d3 to 9){
            for (perm1 <- permutations(List(d1, d2, d3, d4))){
              val m1 = mn(perm1)
              for (perm2 <- permutations(List(d1, d2, d3, d4))){
                val m2 = mn(perm2)
                for (perm3 <- permutations(List(d1, d2, d3, d4))){
                  val m3 = mn(perm3)
                  if (isPrime(m1) && isPrime(m2) && isPrime(m3) && m1> m2 && m2 > m3 && m1-m2 == m2-m3 && m2 != 4817
                  && m3 > 1000)
                    return m3 * 100000000L + m2*10000 + m1;
                }
              }
            }
          }
        }
      }
    }
    return 0;
  }

  def mn(n : List[Int]) : Int = n(0)*1000+n(1)*100+n(2)*10+n(3)

  def isPrime(n : Int) : Boolean = {
    for (i<- 2 to math.sqrt(n).toInt)
      if (n % i == 0)
        return false;
    return true;
  }

def permutations[T](xs: List[T]): List[List[T]] = xs match {
          case Nil => List(Nil)
          case _   => for(x <- xs;ys <- permutations(xs diff List(x))) yield x::ys
      }

}
