package CoinPiles

class Oracle {
  def Answer() : BigInt = {
    var ii = 0L
    var n = BigInt(1)
    while(n % 1000000 != 0){
      ii += 1
      n = Fe(ii)
    }
    ii
  }
  
//  var m = Array.ofDim[BigInt](10000, 10000)
//
//  def SumF(n:Long,  f:Long) : Long = {
//
//    if(n == 0)
//      return 1
//    if(f > n)
//      return 0
//
//    val sum = Stream.from(f.toInt).takeWhile(u => u <= n).map(u => SumF(n - u, u)).sum
//    sum
//  }

  //var mm = Array.ofDim[BigInt](100000000)
  
  var mm = collection.mutable.HashMap[Long,  BigInt]()

  def Fe(n:Long) : BigInt = {
    if(n>=0 && mm.contains(n))
      return mm(n)
    if(n==0)
      return BigInt(1)
    if(n<0)
      return BigInt(0)
    var sum = BigInt(0)
    var kk = 1L
    while(kk <= n){
      val n1 = n - kk*(3*kk-1)/2
      val n2 = n - kk*(3*kk+1)/2
      sum += (Fe(n1)+Fe(n2))*(if (kk %2 ==0) -1 else 1)
      kk += 1
    }

    if(sum % 1000 == 0)
      System.out.println(sum)
    mm(n) = sum
    sum
  }
  
//  def Filla(){
//    for(i <- 1 to m.length - 1; j <- 1 to m.length-1)
//      m(i)(j) = BigInt(0)
//    for(i <- 0 to  m.length - 1){
//      m(0)(i) = 1
//      m(i)(i) = 1
//    }
//
//    for (n <- 1 to m.length-1){
//
//      for(f <- (n to 1 by -1) ){
//        var sum = BigInt(0)
//        for(i <- f to n)
//          sum += m(n-i)(i)
//        m(n)(f) = sum
//      }
//
//      if(m(n)(1)%1000 == 0)
//        System.out.println(m(n)(1))
//    }
//  }


}
