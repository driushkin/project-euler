package SquareCount

class Oracle {
  def Answer() : Int = {
    var nearest = 0
    var nrm = 0
    var nrn = 0

    Stream.from(1).takeWhile(u => SquareNum(u, 1) < 2001000).foreach(m => {
      Stream.from(1).takeWhile(u => SquareNum(m, u)< 2001000).foreach(n => {
        val num = SquareNum(m, n)
        if(math.abs(num-2000000) < math.abs(nearest-2000000)){
          nearest = num
          nrm=m
          nrn=n
        }
      })
    })
    nrm*nrn
  }

  def SquareNum(m : Int, n: Int) : Int = {
    var sum = 0
    for(ni <- 1 to n){
      for(mi <- 1 to m) {
        sum += ni*mi
      }
    }
    sum
  }

}
