package SquareCount

import org.scalatest.FunSpec


class OracleSpec extends FunSpec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 2772)
    }

    it("3x3"){
      assert(oracle.SquareNum(4, 2) === 30)
    }

    it("1x2"){
      assert(oracle.SquareNum(1, 2) === 3)
    }

    it("1x3"){
      assert(oracle.SquareNum(1, 3) === 6)
    }

    it("2x3"){
      assert(oracle.SquareNum(2, 3) === 18)
    }

  }
}