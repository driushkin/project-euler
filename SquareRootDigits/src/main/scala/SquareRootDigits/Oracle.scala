package SquareRootDigits

class Oracle {
  def Answer() : Int = {
    (1 to 100).filter(u => !IsSquare(u)).map(u=>Approx(u, 102)).map(u=>u.toString)
    .map(u=> u.take(100).map(u=>u-'0').sum ).sum
  }

  def Approx(n :Int, prec : Int) : BigInt =
    Approx(BigInt(10).pow(prec), BigInt(10).pow(prec)*n, n, prec )
  
  def Approx(from : BigInt, to : BigInt, n : Int, prec : Int) : BigInt = {
    if((from - to).abs <= 1){
      if(from == to)
        return from;
      val target = BigInt(10).pow(prec*2)*n
      val mid = (from + to)/2
      val sq = mid * mid
      if (target > sq)
        return to
      else
        return from
    }
    val target = BigInt(10).pow(prec*2)*n
    val mid = (from + to)/2
    val sq = mid * mid
    if (target > sq)
      return Approx(mid, to, n, prec)
    else
      return Approx(from, mid, n, prec)
  }
  
  def IsSquare(n : Int) : Boolean = {
    val sq = math.sqrt(n).round.toInt
    sq*sq == n
  }

}
