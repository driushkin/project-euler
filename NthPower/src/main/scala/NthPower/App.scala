package NthPower

class Oracle {
  def Answer() : Int = {
    var sum = 0
    for (n <- 1 to 21){
      sum +=Stream.from(1).map(u=>math.pow(u, n).round).dropWhile( u => u.toString.length() < n )
        .takeWhile(u => u.toString.length()==n ).length
    }
    sum
  }

}
