package TrianglePath

class Oracle {
  var data = Array[Array[Int]]()
  var datam = Array[Array[Int]]()

  def Answer() : Int = {
    ReadData()
    for(x <- 0 to data.length-1){
      var ln = data(x)
      if (x == 0 )
        datam = datam :+ Array[Int](data(0)(0))
      else {
        datam = datam :+ Array.fill(ln.length)(0)
        for (y <- 0 to ln.length-1){
          var m = 0
          if(y==0)
            m = datam(x-1)(y)
          else if (y==ln.length-1)
            m = datam(x-1)(y-1)
          else m = math.max(datam(x-1)(y-1), datam(x-1)(y))
          datam(x)(y) = data(x)(y)+m
        }
      }
    }
    datam(datam.length-1).max
  }

  def ReadData() {
    val lines = io.Source.fromFile("triangle.txt").getLines()
    for(val line <- lines){
      val ll = line.split(" ").map(u => u.toInt).toArray
      data = data :+ ll
    }
  }

}
