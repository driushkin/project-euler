package NameScore

import util.Sorting;

class Oracle  {
  def Answer(names : Array[String]) : Int = {
    Sorting.quickSort(names)
    names.zipWithIndex
            .foldLeft(0)((u, v) => u + (v._1.map(u=>u.toInt-'A'+1).reduceLeft(_+_))*(v._2+1) )
  }
}
