package NameScore

import org.scalatest.Spec
import java.util.Scanner
import java.io.{FileInputStream, FileReader};

class OracleTest extends Spec{
  describe("Oracle"){
    val oracle = new Oracle()
    it("['A'] => 1"){
      var array = Array("A")
      assert(oracle.Answer(array)=== 1 )
    }
    it("['B'] => 2"){
      var array = Array("B")
      assert(oracle.Answer(array)=== 2 )
    }

    it("['C'] => 3"){
      var array = Array("C")
      assert(oracle.Answer(array)=== 3 )
    }

    it("['A', 'A'] => 3"){
      var array = Array("A", "A")
      assert(oracle.Answer(array)=== 3 )
    }

    it("['A', 'B'] => 5"){
      var array = Array("A", "B")
      assert(oracle.Answer(array)=== 5 )
    }

    it("['B', 'A'] => 5"){
      var array = Array("B", "A")
      assert(oracle.Answer(array)=== 5 )
    }

    it("['COLIN'] => 53"){
      var array = Array("COLIN")
      assert(oracle.Answer(array)=== 53 )
    }

    it("my answer!"){
      val text = new StringBuilder();
      val scanner = new Scanner(new FileInputStream("e:\\names.txt") );
      try {
        while (scanner.hasNextLine()){
          text.append(scanner.nextLine());
        }
      }
      finally{
        scanner.close();
      }
      val tt = text.toString
      val strings = tt.split(',')

      var array = (strings.map(u=> u.substring(1, u.length-1))).toArray
      assert(oracle.Answer(array) === 0)
    }

  }
}
