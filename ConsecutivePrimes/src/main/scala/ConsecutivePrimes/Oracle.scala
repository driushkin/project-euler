package ConsecutivePrimes;

class Oracle {
  def Answer() : Int = {
    var startPrime = 2
    var maxLen = 1
    var maxPrime = 2

    while(startPrime < 1000000){
      var sum = startPrime
      var curLen = 1
      var curPrime = nextPrime(startPrime)
      while(sum < 1000000){
        sum += curPrime
        curLen += 1
        if(isPrime(sum)){
          if (curLen > maxLen){
            maxLen = curLen
            maxPrime = sum
          }
        }
        curPrime = nextPrime(curPrime)
      }

      startPrime = nextPrime(startPrime)
    }

    return maxPrime;
  }

  def nextPrime(n: Int) : Int = {
    var nn = n + 1
    while(! isPrime(nn))
      nn+= 1
    return nn
  }

  def isPrime(n: Int) : Boolean = {
    for (i <- 2 to  math.sqrt(n).toInt)
      if (n%i==0)
        return false;
    return true;
  }
}
