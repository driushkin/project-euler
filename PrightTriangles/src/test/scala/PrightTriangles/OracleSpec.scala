package PrightTriangles

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer(50) === 14234)
    }
    it("rt"){
      assert(oracle.RightTriangle(0, 2, 1, 0) === true)
      assert(oracle.RightTriangle(1, 1, 2, 1) === false)
      assert(oracle.RightTriangle(1, 1, 2, 0) === true)
    }
  }
}