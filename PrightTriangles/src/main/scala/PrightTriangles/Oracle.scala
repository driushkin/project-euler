package PrightTriangles

class Oracle {
  def Answer(n : Int) : Int = {
    (for (x1 <- 0 to n; y1 <- 0 to n; x2 <- 0 to n; y2 <- 0 to n) yield (x1,y1,x2,y2) )
      .filter(u=>RightTriangle(u._1, u._2, u._3, u._4)).length / 2
  }

  def RightTriangle(x1 : Int, y1 : Int, x2 : Int, y2 : Int) : Boolean = {
    if ((x1==0 && y1 == 0) || (x2==0 && y2==0) || x1 == x2 && y1 == y2)
      return false
    val d1 = x1 * x1 + y1 * y1
    val d2 = x2 * x2 + y2 * y2
    val d3 = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
    d1+d2==d3 || d1+d3==d2 || d2+d3==d1
  }

}
