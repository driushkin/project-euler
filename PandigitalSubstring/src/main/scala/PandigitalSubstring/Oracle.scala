package PandigitalSubstring;


class Oracle {
  def Answer() : Long ={
    var sum = 0L
    var i = 1234567890L;
    while(i< 9999999999L){
      if(HasInterestingProperty(i) && IsPandigital10(i) )
        sum += i;
      i+= 1;
      if (i%100000000 == 0){
        System.out.println(i)
      }
    }

    return sum;
  }

  def IsPandigital10(n: Long): Boolean ={
    if(n < 1234567890L)
      return false;
    var r = 0
    var nn = n
    while(nn > 0){
      val m = nn % 10
      r |= (1 << m)
      nn = nn / 10
    }
    return r == 0x3FF
  }

  def HasInterestingProperty(n : Long) : Boolean = {
    return (n%1000)%17 == 0 &&
    n/10%1000%13 == 0 &&
    n/100%1000%11 == 0 &&
    n/1000%1000%7 == 0 &&
    n/10000%1000%5 == 0 &&
    n/100000%1000%3 == 0 &&
    n/1000000%1000%2 == 0;
  }

}
