package Lychrel

class Oracle {
  def Answer() : Int =
    (1 to 10000).filter(u => IsLychrel(u)).length
  
  def IsLychrel(n : BigInt) : Boolean = {
    Seq.iterate(NextPaly(n), 50) (u => NextPaly(u))
      .filter(u=> IsPalyndrom(u))
      .length == 0
  }
  
  def NextPaly(n : BigInt) : BigInt =
    n + BigInt(n.toString().reverse)
  
  def IsPalyndrom(n : BigInt) : Boolean =
    n.toString() == n.toString().reverse
  
}
