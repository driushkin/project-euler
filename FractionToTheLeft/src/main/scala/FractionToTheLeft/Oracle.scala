package FractionToTheLeft

class Oracle {
  def Answer() : Int = {
    (8 to 1000000).par.map(u => (NearestNom(u), u)).minBy(u => 3.0/7.0-u._1.toDouble/u._2.toDouble)._1
  }

  def NearestNom(n : Int): Int= {
    var target = (n *3.0/7.0).toInt
    while(!IsRelPrime(n, target)){
      target -= 1
      if (target == 0)
        return 1
    }
    target
  }
  
  def Gcd(a:Int,  b : Int) : Int = {
    if (a%b==0)
      b
    else Gcd(b, a%b)
  }
  
  def IsRelPrime(a: Int,  b : Int) : Boolean = {
    Gcd(a, b) == 1
  }

}
