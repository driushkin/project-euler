package PE84_Monopoly

import org.scalatest.FunSpec

class OracleSpec extends FunSpec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === "101524")
    }
  }
}