package PE84_Monopoly

class Oracle {
  def Answer() : String = {
    val game = new Monopoly()
    for( i<- 1 to 5000000) {
      game.Step()
    }
    game.stats.zipWithIndex.sortBy((u: (Int, Int)) => u._1).reverse.take(3)
      .foldLeft("")( (u, v) => u+("%02d" format v._2) )
  }
}









