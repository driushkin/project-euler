package PE84_Monopoly

abstract class Chance {
  var cards: List[String]
  var counter = 0

  def nextPos(curpos: Int): Int = {
    counter = counter + 1
    if (counter == 16) {
      counter = 0
      cards = util.Random.shuffle(cards)
    }

    val h = cards.head
    cards = cards.tail ::: List[String](cards.head)
    select(h, curpos)
  }

  def select(curpos: Int): Int

}
