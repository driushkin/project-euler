package PE84_Monopoly

class CH extends Chance {
  var cards = util.Random.shuffle(List[String]("G", "J", "C1", "E3", "H2", "R1", "R", "R", "U", "-3", "", "", "", "", "", ""))

  def select(h: String, curpos: Int): Int =
    h match {
      case "G" => return 0
      case "J" => return 10
      case "C1" => return 11
      case "E3" => return 24
      case "H2" => return 39
      case "R1" => return 5
      case "R" =>
        curpos match {
          case 7 => return 15
          case 22 => return 25
          case 36 => return 5
        }
      case "U" =>
        curpos match {
          case 7 => return 12
          case 22 => return 28
          case 36 => return 12
        }
      case "-3" => return curpos - 3
      case "" => return curpos
    }
}
