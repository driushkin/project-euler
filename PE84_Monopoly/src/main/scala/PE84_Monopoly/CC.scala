package PE84_Monopoly

class CC extends Chance {
  var cards = util.Random.shuffle(List[String]("G", "J", "", "", "", "", "", "", "", "", "", "", "", "", "", ""))

  def select(h: String, curpos: Int): Int =
    h match {
      case "G" => 0
      case "J" => 10
      case _ => curpos
    }

}
