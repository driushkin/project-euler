package PE84_Monopoly

import util.Random

class Monopoly {
  val stats = Array.ofDim[Int](40)
  val rng = new Random()

  var curpos = 0
  var lastdouble = 0
  var lcount = 0

  val cc = new CC()
  val ch = new CH()

  def Step() {
    val dice1 = rng.nextInt(4) + 1
    val dice2 = rng.nextInt(4) + 1
    val dice = dice1 + dice2
    if (dice1 == dice2) {
      if (lastdouble == dice1) {
        lcount = lcount + 1
      }
      else {
        lastdouble = dice1
        lcount = lcount + 1
      }
    }
    else {
      lcount = 0
    }
    curpos = (curpos + dice) % 40

    if (lcount == 3) {
      curpos = 10
      lcount = 0
    }

    if (curpos == 2 || curpos == 17 || curpos == 33) {
      curpos = cc.nextPos(curpos)
    }

    if (curpos == 7 || curpos == 22 || curpos == 36) {
      curpos = ch.nextPos(curpos)
    }

    if (curpos == 30)
      curpos = 10

    stats(curpos) = stats(curpos) + 1
  }

}
