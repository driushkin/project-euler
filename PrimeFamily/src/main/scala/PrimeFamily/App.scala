package PrimeFamily;

class Oracle{

  def Answer() : Int = {
    return Stream.from(2).filter(u => isPrime(u))
      .map(u => MaxFamily(u.toString))
      .filter(u => u._1 >= 8).head._2
  }

  def MaxFamily(n: String) : (Int, Int) = {
    return (1 to  n.length()).map(u =>
      GeneratePatterns(n, u).map(v => CalculatePrimes(v))
      ).flatten.foldLeft( (0, 0) ) ((u, v) => if (v._1 > u._1) v else u )
  }

  def CalculatePrimes(pattern : String) : (Int, Int) = {
    val primes = (0 to 9).map(u=> pattern.replace('*', ('0'.intValue()+u).toChar ) )
      .filter(u => u(0) != '0')
      .map( u => u.toInt)
      .filter ( u=> isPrime(u));

    return(primes.length, if(primes.length > 0) primes(0) else 0)
  }

  def GeneratePatterns(n : String, positions : Int) : Seq[String] = {
    if (positions == 0)
      return List[String](n);

    return (0 to  n.length() - positions).map(u =>
      GeneratePatterns(n.substring(u+1, n.length()), positions - 1)
        .map(v => n.substring(0, u)+"*" + v )
    ).flatten
  }

  def isPrime(n : Int) : Boolean = {
    for ( i <- 2 to math.sqrt(n).toInt )
      if( n % i == 0)
        return false;
    return true;
  }
}
