package PrimeFamily

import org.scalatest.Spec
;


class OracleSpec extends Spec {
  describe("oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 121313)
    }
  }
}
