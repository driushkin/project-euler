package RightSidedTriangles

class Oracle {
  def Answer() : Int = {
    (1 to 1500000).par.filter(u => RightTriangleCount(u) == 1).length
  }
  
  def RightTriangleCount(n : Int) : Int = {
    var sum = 0
    var up = 0
    var upper = n/(2+math.sqrt(2) )
    if((upper - upper.round).abs < 0.00001)
      up = upper.round.toInt
    else
      up = upper.toInt
    val nn = n.toLong
    for(a <- 1 to up){
      if((nn*nn-2*nn*a)%(2*(nn-a)) == 0)
        sum += 1
    }
    if(n % 10000 == 0){
      System.out.println(n)
    }
    sum
  }
  
}
