package Sqrt2Periods

class Oracle {
  def Answer() : Int = {
    return (1 to 10000).filter(u => PeriodLength(u)%2!=0).length
  }
  
  def PeriodLength(nn:Int) : Int = {
    val ns = math.sqrt(nn).toInt
    if(ns*ns==nn)
      return 0;
    var n = ns
    var a=n
    var b=1
    
    var r = List[(Int,  Int,  Int)]()
    while(true){
      b = (nn - a*a)/b
      n = (ns + a)/b
      a = n*b-a
      if (r.contains((n, a, b)))
        return r.length - r.indexOf(n, a, b)
      r = r :+ (n,  a,  b)
    }
    0
  }
  
}
