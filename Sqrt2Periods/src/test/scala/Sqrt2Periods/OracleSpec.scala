package Sqrt2Periods

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 1322)
    }
    it("try"){
      assert(oracle.PeriodLength(2) === 1)
      assert(oracle.PeriodLength(3) === 2)
      assert(oracle.PeriodLength(4) === 0)
      assert(oracle.PeriodLength(5) === 1)
      assert(oracle.PeriodLength(6) === 2)
      assert(oracle.PeriodLength(7) === 4)
      assert(oracle.PeriodLength(8) === 2)
      assert(oracle.PeriodLength(9) === 0)
      assert(oracle.PeriodLength(10) === 1)
      assert(oracle.PeriodLength(11) === 2)
      assert(oracle.PeriodLength(12) === 2)
      assert(oracle.PeriodLength(13) === 5)
      assert(oracle.PeriodLength(23) === 4)
    }
  }
}