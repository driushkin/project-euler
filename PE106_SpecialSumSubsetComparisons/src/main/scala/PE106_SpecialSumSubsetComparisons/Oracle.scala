package PE106_SpecialSumSubsetComparisons

class Oracle {
  def Answer() : Int = {
    val l = (1 to 12).toList
    EqualSubsets(l).count(ss => NeedCheckSubset(ss._1, ss._2))/2
  }

  def EqualSubsets(n : List[Int]) : Seq[(List[Int], List[Int])] = {
    (1 to n.length / 2).flatMap(i => {
      val combs = n.combinations(i).toList
      for (c1 <- combs; c2 <- combs; if (c1 intersect c2).isEmpty)
        yield (c1, c2)
    } )
  }

  def NeedCheckSubset(ss1 : List[Int], ss2 : List[Int]) : Boolean = {
    val smax = if (ss1.max > ss2.max) ss1 else ss2
    val smin = if (ss1.max < ss2.max) ss1 else ss2

    (smax zip smin).exists(e => e._1 < e._2)
  }

}
