package SquareCubes

class Oracle {
  def Answer() : Int = {

    val l = List[Int](0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    val c1 = l.combinations(6).toList
    val c2 = l.combinations(6).toList

    (for (cc1 <- c1; cc2 <- c2)
      yield (cc1, cc2)).filter(u => CombiFit(u._1, u._2)).length
  }

  def NumFit(c1: List[Int], c2: List[Int], n1: Int, n2: Int) : Boolean =
    (c1.contains(n1) && c2.contains(n2)) || (c1.contains(n2) && c2.contains(n1))

  def CombiFit(c1 : List[Int], c2 : List[Int]) : Boolean =
     NumFit(c1, c2, 0, 1) &&
     NumFit(c1, c2, 0, 4) &&
     (NumFit(c1, c2, 0, 9) || NumFit(c1, c2, 0, 6)) &&
     (NumFit(c1, c2, 1, 6) ||  NumFit(c1, c2, 1, 9))&&
     NumFit(c1, c2, 2, 5) &&
     (NumFit(c1, c2, 3, 6) || NumFit(c1, c2, 3, 9)) &&
     (NumFit(c1, c2, 4, 9) ||NumFit(c1, c2, 4, 6)) &&
     (NumFit(c1, c2, 6, 4) || NumFit(c1, c2, 9, 4)) &&
     NumFit(c1, c2, 8, 1)


}
