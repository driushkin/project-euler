package FibonacciPandigital

class Oracle {
  def Answer() : Int = {
    Fibonacci.zipWithIndex.filter(u => IsPandigital(u._1)).head._2 + 1
  }

  var nn = 0

  def IsPandigital(n : BigInt) : Boolean = {
    nn += 1
    if (nn % 1000 == 0)
      println(nn)

    val s = n.toString()

    val sstart = s.take(9).distinct
    val send = s.takeRight(9).distinct
    if ( sstart.length == 9 && !sstart.contains('0')
      && send.length == 9 && !send.contains('0'))
      return true
    false
  }

  def Fibonacci : Stream[BigInt] =
    for( n <- Stream.iterate((BigInt(1), BigInt(1)))(u => (u._2, u._1+u._2 ) ))
      yield n._1

}
