package FactorialChains

class Oracle {
  def Answer() : Int = {
    (1 to 1000000).filter(u => ChainLength(u) == 60).length
  }
  
  def ChainLength(n : Int) : Int = {
    var r = List[Int](n)
    var nn = n
    while(true){
      nn = NextTerm(nn)
      if(r.contains(nn))
        return r.length
      else
        r = r :+ nn
    }
    throw new Exception()
  }
  
  def NextTerm(n : Int) : Int = {
    var sum = 0
    var nn = n
    while(nn > 0){
      sum += Fact(nn%10)
      nn = nn / 10
    }
    sum
  }
  
  def Fact(n : Int) : Int =
    (1 to n).foldLeft(1)(_*_)
  
}
