package PandigitalPrime;

class Oracle {
  def Answer(): Int = {
    var max = 0
    for (i<- 1 to 9999999){
      if (IsPandigital7(i) && IsPrime(i)){
        max = i
      }
    }
    return max
  }

  def IsPandigital7(n: Int): Boolean ={
    if(n < 1234567)
      return false;
    var r = 0
    var nn = n
    while(nn > 0){
      val m = nn % 10
      if(m==0)
        return false;
      r |= (1 << (m-1))
      nn = nn / 10
    }
    return r == 0x7F
  }

  def IsPrime(n:Int) : Boolean = {
    for (i <- 2 to  math.sqrt(n).toInt){
      if(n % i == 0)
        return false;
    }
    return true;
  }

}
