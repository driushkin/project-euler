package DigitSquareChain

class Oracle {
  def answer() : Int =
    (1 until 10000000).par.filter(u => numberChainResolution(u) == 89).length

  def next(n: Int): Int =
    n.toString.map(d => d.toInt-'0'.toInt).map(d=>d*d).sum

  def numberChainResolution(n : Int) = {
    var nn = n
    while(nn != 1 && nn != 89)
      nn = next(nn)
    nn
  }

}
