package MassivePrimeLastDigits

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      val n = (BigInt(2).modPow(7830457, 10000000000L) * 28433 + 1).mod(10000000000L)
      assert(n === 8739992577L)
    }
  }
}