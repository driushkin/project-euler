package pandigital

import org.scalatest.Spec

class OracleSpec extends Spec {

  def countWords(text:String) = {
    val counts = Map.empty[String, Int]
    for (rawWord <- text.split("[ ,!.]+")){
      val word = rawWord.toLowerCase
      val oldCount = if (counts.contains(word)) counts(word) else 0
      counts(word) = oldCount + 1
    }
    counts
  }

  describe("Oracle"){
    val oracle = new Oracle()
    it("my question"){
      val res = oracle.Answer()
      assert(res === 932718654)
    }

    it("pandigital"){
      assert(oracle.IsPandigital9(123456789) === true)
      assert(oracle.IsPandigital9(12345678) === false)
      assert(oracle.IsPandigital9(139456728) === true)
      assert(oracle.IsPandigital9(1394567285) === true)
    }

    it("constructNum"){
      assert(oracle.constructNum(12, 2) === 1224 )
    }

  }



}

