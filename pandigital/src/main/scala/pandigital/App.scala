package pandigital;

import scala.collection.mutable

class Oracle{
  def Answer() : Int = {
    var max = 0
    for (n <- 2 to 9){
      var i = 1
      var less = true
      while(less){
        val num = constructNum(i, n)
        if(num > 999999999)
          less = false;
        if(IsPandigital9(num))
          if(num > max)
            max = num;
        i += 1
      }
    }
    return max;
  }

  def constructNum(s: Int, n : Int) : Int = {
    var res = 0
    for(i <- 1 to n){
      val cn = s * i
      val m = math.log10(cn).toInt + 1
      val mp = math.pow(10, m).toInt
      res = res * mp + cn
    }
    return res
  }

  def IsPandigital9(n: Int): Boolean ={
    if(n < 123456789)
      return false;
    var r = 0
    var nn = n
    while(nn > 0){
      val m = nn % 10
      if(m==0)
        return false;
      r |= (1 << (m-1))
      nn = nn / 10
    }
    return r == 0x1FF
  }

}