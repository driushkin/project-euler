package PolynomialApproximations

class Oracle {
  def Answer() : Long = {
    (1 to 10).map(i => ApproxSequence(i).zip(OriginalSequence()).dropWhile(u => u._1 == u._2).head._1)
      .sum
  }

  def EvalPoly(l: Array[Long], n: Int): Long =
    l.foldLeft((1L, 0L))((a, el) => (a._1*n,  a._1*el + a._2) )._2

  def ApproxSequence(n : Int) : Stream[Long] = {
    val m = MakeMatrix(n)
    val s = SolveMatrix(m)
    for (i <- Stream.from(1))
      yield EvalPoly(s, i)
  }

  def OriginalSequence() : Stream[Long] = {
    for (i <-Stream.from(1))
      yield GeneratingFn(i)
  }

  def GeneratingFn(n : Long) : Long =
    1 - n + n*n - n*n*n + n*n*n*n - n*n*n*n*n + n*n*n*n*n*n - n*n*n*n*n*n*n + n*n*n*n*n*n*n*n - n*n*n*n*n*n*n*n*n +
      n*n*n*n*n*n*n*n*n*n

  def MakeMatrix(n: Int) : Array[Array[Long]] =
    Array.tabulate(n, n+1) ((x, y) => if (y!= n) math.pow((x+1), y).toLong else GeneratingFn((x+1)) )

  def SolveMatrix(mm : Array[Array[Long]]) : Array[Long] = {
    val n = mm.length

    for (i <- 0 to n-2) {
      if (mm(i)(i) > 1){
        val d = mm(i)(i)
        for (k <- 0 to n)
          mm(i)(k) = mm(i)(k)/d
      }

      for (j <- i + 1 to n-1) {
        val d = mm(j)(i)
        for (t <- 0 to n) {
          mm(j)(t) = mm(j)(t) - d * mm(i)(t)
        }
      }
    }

    for (i <- n-1 to 1 by -1) {
      if (mm(i)(i) > 1){
        val d = mm(i)(i)
        for (k <- 0 to n)
          mm(i)(k) = mm(i)(k)/d
      }
      for (j <- i - 1 to 0 by -1) {
        val d =  mm(j)(i)
        for (t <- 0 to n) {
          mm(j)(t) = mm(j)(t) - d * mm(i)(t)
        }
      }
    }

    (0 to n - 1).map(u => mm(u)(n)).toArray
  }

}
