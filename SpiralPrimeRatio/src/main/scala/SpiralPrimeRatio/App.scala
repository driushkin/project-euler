package SpiralPrimeRatio

class Oracle {
  def Answer(): Int = {
    var primes = 0
    var i = 0
    var s = 1
    do{
      val step = (i+1)*2
      if(IsPrime(s+step))primes += 1
      if(IsPrime(s+2*step))primes += 1
      if(IsPrime(s+3*step))primes += 1
      if(IsPrime(s+4*step))primes += 1

      s+= 4 * step
      i += 1
    }while(primes/ (1.0+i*4) > 0.1)
    
    //Stream.from(0).foldLeft(1)((u, v) => u + 8*(1+v) )

    return 1 + 2*i;
  }
  
  def IsPrime(n: Int) : Boolean = {
    for (i <- 2 to math.sqrt(n).toInt )
      if (n%i == 0)
        return false;
    return true;
  } 

}
