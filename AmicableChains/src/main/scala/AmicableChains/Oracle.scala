package AmicableChains

import collection.mutable

class Oracle {
  def Answer() : Int = {
    (1 to 1000000).map(n => ChainMin(n)).filter(n=> !n.isEmpty).map(n=> n.get).maxBy(n=> n._1)._2
  }

  val s = mutable.HashSet[Int]()

  def ChainMin(n: Int) : Option[(Int, Int)] = {
    if (s.contains(n))
      return Option.empty

    val srun = mutable.HashMap[Int, Int]()

    var nn = n
    for (i <- Stream.from(1)) {
      srun.+=((nn, i))
      s.+=(nn)
      nn = Next(nn)
      if (nn > 1000000)
        return Option.empty
      if(srun.contains(nn)){
        return Option((i-srun.get(nn).get+1 ,Min(nn)))
      }
    }
    Option.empty
  }

  def Divisors(n : Int) : List[Int] = {
    var d = List[Int]()
    var nn = n
    for (i <- 2 to math.sqrt(n).toInt){
      while(nn % i == 0 ) {
        d = i :: d
        nn = nn / i
      }
    }
    if (nn == 1) d else nn :: d
  }

  def ProperDivisors(n : Int) : List[Int] = {
    val ds = Divisors(n)
    var dd = mutable.HashSet[Int](1)
    for (i <- 1 to ds.length-1){
      dd.++= (ds.combinations(i).map(u=> u.product))
    }
    dd.iterator.toList
  }

  def Min(n: Int ): Int = {
    var min = n
    var nn = Next(n)
    while(nn != n){
      if (nn < min)
        min = nn
      if (nn > 1000000)
        return Int.MaxValue
      nn = Next(nn)
    }
    min
  }

  var cm = mutable.HashMap[Int, Int]()

  def Next(n: Int) : Int = {
    if (cm.contains(n))
      return cm.get(n).get
    val c = ProperDivisors(n).sum
    cm.+=((n, c))
    c
  }


}
