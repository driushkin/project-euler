package Diophantine

class Oracle {
  def Answer() : Int = {
    (1 to  100000).filter(u => math.sqrt(u).round*math.sqrt(u).round != u)
      .map(u => (u, MinDiophantine(u))).toList.maxBy(u => u._2)._1
  }
  
  def MinDiophantine(d : Int) : BigInt =
    DiophSolution(d)._1

  def Satisfies(h : BigInt, k : BigInt, d : Int) : Boolean =
    h*h-k*k*d == 1

  def ExpandContinuousFraction(l : List[Int]) : (BigInt,  BigInt) = {
    l.drop(1).foldLeft((BigInt(l.head), BigInt(1)))((u, v) => (u._1*v + u._2, u._1) )
  }

  def DiophSolution(d:Int) : (BigInt, BigInt) = {
      val ns = math.sqrt(d).toInt
      var n = ns
      var a=n
      var b=1

      var r = List[Int](ns)
      while(true){
        b = (d - a*a)/b
        n = (ns + a)/b
        a = n*b-a

        r = n +: r
        val exp = ExpandContinuousFraction(r)
        if (Satisfies(exp._1, exp._2, d))
          return exp
      }
      throw new Exception()
    }


}
