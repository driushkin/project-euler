package SquareWordAnagrams

class Oracle {

  def AreAnagrams(w1: String, w2: String): Boolean = {
    (w1 != w2 && w1.sorted == w2.sorted)
  }

  def MaxSquare(letters : List[Char], numbers: List[Int], w1: String, w2: String) : Int = {
    if (letters.isEmpty) {
      if (w1.startsWith("0") || w2.startsWith("0"))
        return 0
      if (IsSquare(w1.toInt) && IsSquare(w2.toInt))
        return math.max(w1.toInt, w2.toInt)
      return 0
    }
    numbers.map( n => MaxSquare(letters.tail, numbers.filter(u => u != n),
        w1.replace(letters.head, n.toString()(0)), w2.replace(letters.head, n.toString()(0)))
    ).max
  }

  def IsSquare(n : Int) : Boolean = {
    val nn = math.sqrt(n).toInt
    nn * nn == n
  }

  def Answer() : Int = {
    val words = io.Source.fromFile("words.txt").mkString.split(',').map(u=> u.stripPrefix("\"").stripSuffix("\""))
    (for (w1 <- words; w2 <- words) yield (w1, w2))
      .filter(u => AreAnagrams(u._1, u._2))
      .filter(u => u._1 < u._2)
      .map(u => MaxSquare(u._1.toList, (0 to 9).toList, u._1, u._2)).max
  }

}
