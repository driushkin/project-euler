package DiscCombinations

class Oracle {

  def IntRootb(n: Long) : Option[Long] = {
    val nn = BigInt(n)
    val det = 4 + 8 * (nn * nn - nn)
    val sq = Sqrt(det)
    if (!sq.isEmpty) {
      val sq2 = 2 + sq.get
      if (sq2 % 4 == 0)
        return Option(sq2 / 4)
    }
    Option.empty
  }


  def Sqrt(n : BigInt) : Option[Long] = {
    val sq = math.sqrt(n.toDouble).toLong
    var bq = BigInt(sq)
    while(bq*bq <= n) {
      if (bq*bq==n)
        return Option(bq.toLong)
      bq+= 1
    }
    while(bq*bq >= n) {
      if (bq*bq==n)
        return Option(bq.toLong)
      bq-= 1
    }
    Option.empty
  }

  def Answer() : (Long, Long) = {
    Stream.iterate(1070375362035L)(u => u + 1)
      .map(u => (u, IntRootb(u)))
      .filter(n => !n._2.isEmpty).map(u => (u._1, u._2.get)).head
  }
}
