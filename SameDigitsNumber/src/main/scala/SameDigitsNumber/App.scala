package SameDigitsNumber

class Oracle {
  def Answer() : Int = {
    Stream.from(1)
      .filter(u => CheckNum(u))
    .head
  }

  def CheckNum(n : Int) : Boolean = {
    val dm1 = DigitMask(n)
    val dm2 = DigitMask(2*n)
    val dm3 = DigitMask(3*n)
    val dm4 = DigitMask(4*n)
    val dm5 = DigitMask(5*n)
    val dm6 = DigitMask(6*n)
    return ((dm1|dm2|dm3|dm4|dm5|dm6) == dm1)
  }

  def DigitMask(n : Int) : Int = {
    var res = 0
    var nn = n
    while(nn > 0){
      res = res | (1 << (nn % 10) )
      nn = nn / 10
    }

    return res
  }

}
