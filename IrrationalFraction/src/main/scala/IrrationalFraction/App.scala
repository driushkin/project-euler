package IrrationalFraction;

class Oracle {
  def Answer() : Int = {
    var i = 1
    var c = 1
    var res = 1
    var nextnum = 1
    while(i<=1000000){
      val step = math.log10(c).toInt + 1
      if (nextnum >= i && nextnum < i+step){
        val digit = (c / (math.pow(10, step-nextnum+i-1).toInt)) % 10
        res *= digit
        nextnum *= 10
      }
      i += step
      c += 1
    }
    return res
  }
}
