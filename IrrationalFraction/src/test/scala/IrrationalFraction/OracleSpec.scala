package IrrationalFraction

import org.scalatest.Spec
;

class OracleSpec extends Spec{
  describe("Oracle"){
    val oracle = new Oracle()
    it("My Answer!"){
      assert(oracle.Answer() === 210)
    }
  }
}