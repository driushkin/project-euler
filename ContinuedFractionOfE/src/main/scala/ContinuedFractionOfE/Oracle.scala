package ContinuedFractionOfE

class Oracle {
  def Answer() : Int = {
    
    val seq = (List[Int](2) ++ Seq.fill(99)(1).zipWithIndex
      .map(u => if ((u._2 + 2) % 3 == 0) 2 * (u._2 + 2) / 3 else u._1)).reverse
    
    seq.drop(1).foldLeft((BigInt(seq.head), BigInt(1)))((u, v) => (u._1*v + u._2, u._1) )
    ._1.toString.map(u => u.toInt-'0'.toInt).sum
  }

  
}
