package MaxBaseExponent

class Oracle {

  def Exponent(base: Int, exp: Int): Double = {
    math.log(base)*exp
  }

  def Answer() : Int = {
    io.Source.fromFile("base_exp.txt").getLines().map(u=> u.split(",")).map(u => (u(0).toInt, u(1).toInt))
      .map(u => Exponent(u._1, u._2)).zipWithIndex.maxBy(u=> u._1)._2+1
  }
}
