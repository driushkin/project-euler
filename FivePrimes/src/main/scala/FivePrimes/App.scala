package FivePrimes

import collection.mutable.{HashSet, BitSet}

class Oracle  {
  def Answer() : Int = {
    val primes = GenPrimes(27000)

    var sum = 1000000000
    for(p1 <- primes){
      System.out.println(p1+"-")
      for(p2<- primes.dropWhile(u => u <= p1).takeWhile(u => p1+4*u<sum) ){
        if(GoodAppend(List[Int](p1), p2)){
          for (p3 <- primes.dropWhile(u => u <= p2).takeWhile(u=> p1+p2+3*u<sum)){
            if(GoodAppend(List[Int](p1, p2), p3)){
              for (p4 <- primes.dropWhile(u => u <= p3).takeWhile(u=> p1+p2+p3+2*u < sum)){
                if(GoodAppend(List[Int](p1, p2, p3), p4)){
                  for(p5 <- primes.dropWhile(u => u <= p4).takeWhile(u=> p1+p2+p3+p4+u<sum)){
                    if (GoodAppend(List[Int](p1,p2,p3,p4), p5)){
                      if (p1.toInt+p2.toInt+p3.toInt+p4.toInt+p5.toInt < sum){
                        sum = p1.toInt+p2.toInt+p3.toInt+p4.toInt+p5.toInt;
                        System.out.println(p1+" "+p2+" "+p3+" "+p4+ " "+p5)
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    sum
  }
  
  def GoodSet(primes : List[Int]) : Boolean = {
    for(p1 <- primes){
      for(p2 <- primes){
        if(p1 != p2){
          var p1s = p1.toString
          var p2s = p2.toString
          
          if(!IsPrime((p1s+p2s).toInt))
            return false
          if(!IsPrime((p2s+p1s).toInt))
            return false
        }
        else if (primes.indexOf(p1) == primes.indexOf(p2) )
          return false
      }
    }
    return true
  }

  
  def GoodAppend(primes : List[Int], newPrime : Int) : Boolean ={
    val p1s = newPrime.toString
    for(p <- primes){
      if (p == newPrime)
        return false;
      var p2s = p.toString

      if(!IsPrime((p1s+p2s).toLong))
        return false
      if(!IsPrime((p2s+p1s).toLong))
        return false
    }
    return true
  }

  def GoodAppend(primes : List[String], np : String) : Boolean ={
      for(p <- primes){
        if (p == np)
          return false;

        if(!IsPrime((p+np).toLong))
          return false
        if(!IsPrime((np+p).toLong))
          return false
      }
      return true
    }

  def GenPrimes(max : Int): List[Int] = {
    return (2 to  max).filter(u => IsPrime(u)).toList
  }

  def IsPrime(n: Long) : Boolean ={
    for (i <- 2 to math.sqrt(n).toInt )
      if (n%i==0)
        return false;
    return true
  }

  
}
