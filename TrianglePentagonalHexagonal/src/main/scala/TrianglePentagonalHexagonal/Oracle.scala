package TrianglePentagonalHexagonal;

class Oracle {
  def Answer() : Long = {
//    Stream.from(286).map( u => triangle(u))
//      .filter(u => isPentagonal(u) && isHexagonal(u) )
//    .head
    var int = 286
    var inp = 165
    var inh = 143
    while(true){
      val nt = triangle(int)
      val np = pentagonal(inp)
      val nh = hexagonal(inh)
      if(nt==np && nt == nh)
        return nt
      if(nt<=np && nt<=nh) int+= 1
      else if (np<=nt && np<=nh) inp +=1
      else inh +=1
    }
    0
  }

  def triangle(n : Long) : Long = n * (n+1)/2

  def pentagonal (n : Long) : Long = n * (3*n-1)/2

  def hexagonal (n : Long) : Long = n * (2*n -1)

  def isPentagonal(n : Long) : Boolean = {
    val det = 1 + 24*n
    val sqrtdet = math.sqrt(det).toInt

    return (sqrtdet * sqrtdet == det) && ((1 + sqrtdet) %6 == 0)
  }

  def isHexagonal(n : Long) : Boolean = {
    val det = 1 + 8*n
    val sqrtdet = math.sqrt(det).toInt
    return (sqrtdet * sqrtdet == det) && ((1 + sqrtdet) %4 == 0)
  }

}
