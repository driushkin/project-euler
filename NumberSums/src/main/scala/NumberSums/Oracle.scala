package NumberSums

class Oracle {
  def Answer() : Int =
    Stream.from(1).dropWhile(u => SumF(u, 0)<5000).head
  
  var primes = (2 to 100000).filter(u => IsPrime(u))

  def SumF(n:Int,  f:Int) : Int = {
    if(n == 0)
      return 1
    if(primes(f) > n)
      return 0

    Stream.from(f).takeWhile(u => primes(u) <= n).map(u => SumF(n-primes(u), u)).sum
  }
  
  def IsPrime(n : Int) : Boolean = {
    for (i <- 2 to math.sqrt(n).round.toInt )
      if(n%i==0)
        return false
    true
  }
  
}
