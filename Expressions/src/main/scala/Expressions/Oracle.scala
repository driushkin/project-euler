package Expressions

class Oracle {
  def Answer() : Int = {

    val m = (for (a <- 0 to 9; b <- a + 1 to 9; c <- b + 1 to 9; d <- c + 1 to 9)
    yield (a, b, c, d)).maxBy(l => CombValue(List(l._1, l._2, l._3, l._4)))

    m._1*1000+m._2*100+m._3*10+m._4
  }

  def CombValue(comb : List[Int]) : Int =
    Combinations(comb).foldLeft(0)((u, v)=> if(v == u+1) v else u)

  def IsInt(d: Double): Boolean = {
    math.abs(d - d.toInt) < 0.01
  }

  def Combinations(nums : List[Int]) : Array[Int] = {
    val perms = nums.permutations
    val ops = List('+', '-', '*', '/')
    (for (perm <- perms; o1 <- ops; o2 <- ops; o3 <- ops;
         op <- Operations(perm(0), perm(1), perm(2), perm(3), o1, o2, o3))
      yield op).filter(IsInt).map(u=> u.toInt).filter(u=> u > 0).toArray.sorted.distinct
  }

  def Operations(a: Int, b: Int, c: Int, d: Int, o1: Char, o2 : Char, o3 : Char  ) : List[Double] = {
    val r1 = Op(Op(Op(a, b, o1), c, o2), d, o3)
    val r2 = Op(a, Op(Op(b, c, o1), d, o2), o3)
    val r3 = Op(Op(a, Op(b, c, o1), o2), d, o3)
    val r4 = Op(Op(Op(c, d, o1), b, o2), a, o3)
    List(r1, r2, r3, r4)
  }

  def Op(a1: Double, a2 : Double, o : Char) : Double = {
    if (a1 == Int.MaxValue || a2 == Int.MaxValue)
      return Int.MaxValue
    o match {
      case '+' => a1 + a2
      case '-' => a1 - a2
      case '*' => a1 * a2
      case '/' => if(a2 != 0) a1 / a2 else Int.MaxValue
    }
  }

}