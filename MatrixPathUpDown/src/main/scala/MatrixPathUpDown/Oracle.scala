package MatrixPathUpDown

class Oracle {
  def Answer() : Int = {
    ReadData()
    Sweep()
  }

  var m = Array.ofDim[Int](80, 80)

  def ReadData(){
    for((ln, i) <- io.Source.fromFile("matrix.txt").getLines().zipWithIndex ){
      val ll = ln.split(",")
      for(j <- 0 to ll.length - 1)
        m(i)(j) = ll(j).toInt
    }
  }
  
  def Sweep() : Int = {
    var prev = (0 to 79).map(u => m(u)(0)).toArray
    var cur = Array.ofDim[Int](80)
    for(n <- 1 to 79){
      cur = (0 to 79).map(u=> m(u)(n)+prev(u)).toArray
      
      for (mm <- 1 to 80){
        var scur = cur.zipWithIndex.sorted
        for(sel <- scur){
          var i = sel._2
          if(i>0){
            if(cur(i)+m(i-1)(n) < cur(i-1) )
              cur(i-1) = cur(i) + m(i-1)(n)
          }
          if(i< cur.length -1){
            if(cur(i) + m(i+1)(n) < cur(i+1) )
              cur(i+1) = cur(i) + m(i+1)(n)
          }

        }
      }

      prev = cur
    }
    cur.min
  }

}
