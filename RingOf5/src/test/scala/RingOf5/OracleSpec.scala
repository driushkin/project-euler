package RingOf5

import org.scalatest.Spec;

class OracleSpec extends Spec {
  describe("Oracle"){
    val oracle = new Oracle()
    it("my answer!"){
      assert(oracle.Answer() === 6531031914842725L)
    }
  }
}