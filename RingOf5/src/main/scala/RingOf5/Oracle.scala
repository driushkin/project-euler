package RingOf5

class Oracle {
  def Answer() : Long =
    Chk(List[Int](1,2,3,4,5,6,7,8,9,10), List[Int]()).toLong
  
  def Chk(l : List[Int], s : List[Int]) : String={
    if(l.length == 0){
      if(GoodComb(s)){
        val f = Form(s)
        if(f.length() == 16)
          return f
      }
      return ""
    }else
      return l.map(u => Chk(l.filter(v => v != u), s ++ List[Int](u)) ).max
    throw new Exception()
  }
  
  def GoodComb( a : List[Int]): Boolean ={
    val s = a(0)+a(5)+a(6);
    (s==a(1)+a(6)+a(7)&& s==a(2)+a(7)+a(8) && s==a(3)+a(8)+a(9) && s==a(4)+a(9)+a(5))
  }
  
  def Form(a : List[Int]) : String = {
    val r1 = a(0).toString+a(5).toString+a(6).toString
    val r2 = a(1).toString+a(6).toString+a(7).toString
    val r3 = a(2).toString+a(7).toString+a(8).toString
    val r4 = a(3).toString+a(8).toString+a(9).toString
    val r5 = a(4).toString+a(9).toString+a(5).toString
    if (a(0) < a(1) && a(0) < a(2) && a(0) < a(3) && a(0) < a(4)) return r1+r2+r3+r4+r5;
    if (a(1) < a(0) && a(1) < a(2) && a(1) < a(3) && a(1) < a(4)) return r2+r3+r4+r5+r1;
    if (a(2) < a(0) && a(2) < a(1) && a(2) < a(3) && a(2) < a(4)) return r3+r4+r5+r1+r2;
    if (a(3) < a(0) && a(3) < a(1) && a(3) < a(2) && a(3) < a(4)) return r4+r5+r1+r2+r3;
    if (a(4) < a(0) && a(4) < a(1) && a(4) < a(2) && a(4) < a(3)) return r5+r1+r2+r3+r4;

    throw new Exception()
  }
  
}
